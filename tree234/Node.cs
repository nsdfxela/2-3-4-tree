﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tree234
{
    class Node
    {
        public int [] elements { get; set; }
        public Node [] childs { get; set; }
        public int NodeKind 
        {
            get
            {
                if (childs == null) return 0; //not an internal node
                return childs.Length; } 
        }
        /// <summary>
        /// test
        /// </summary>
        public Node(int a, int b, int c) : this(3)
        {
            this.elements[0] = 5;
            this.elements[1] = 7;

            this.childs[0] = new Node(0);
            this.childs[0].elements[0] = 2;

            this.childs[1] = new Node(0);
            this.childs[1].elements[0] = 6;

            this.childs[2] = new Node(0);
            this.childs[2].elements[0] = 8;

        }
        public Node(int nodeType)
        {
            if (nodeType == 2){
                elements = new int[1];
                childs = new Node[2];
            }
            else if (nodeType == 3)
            {
                elements = new int[2];
                childs = new Node[3];
            }
            else if (nodeType == 4)
            {
                elements = new int[3];
                childs = new Node[4];
            }
            //else throw new Exception("Only 2, 3, or 4 nodes allowed!");
            else elements = new int[1];
        }

        public int GetChild(int key)
        {
            if (this.NodeKind == 2)
            {
                if (key < elements[0]) return childs[0].GetChild(key);
                if (key == elements[0]) return elements[0];
                if (key > elements[0]) return childs[1].GetChild(key);
            }
            if (this.NodeKind == 3)
            {
                if (key < elements[0]) return childs[0].GetChild(key);
                if (key == elements[0]) return elements[0];
                if (key > elements[0] & key < elements[1]) return childs[1].GetChild(key);
                if (key == elements[1]) return elements[1];
                if (key > elements[1]) return childs[2].GetChild(key);
            }
            if (this.NodeKind == 4)
            {
                if (key < elements[0]) return childs[0].GetChild(key);
                if (key == elements[0]) return elements[0];
                if (key > elements[0] & key < elements[1]) return childs[1].GetChild(key);
                if (key == elements[1]) return elements[1];
                if (key > elements[1] & key < elements[2]) return childs[2].GetChild(key);
                if (key == elements[2]) return elements[2];
                if (key > elements[2]) return childs[3].GetChild(key);
            }
            int ret = -1;

                foreach (var element in elements)
                {
                    if (element==key)
                    {
                        ret = element;
                    }
                }
                return ret;
        }

        public void Make3(int val)
        {
            int [] newElements = new int[2];
            if (val < this.elements[0])
            {
                newElements[0] = val;
                newElements[1] = elements[0];
            }
            else
            {
                newElements[0] = elements[0];
                newElements[1] = val;
            }
            this.elements = newElements;
            Node [] newChilds = new Node[3];
        }
    }
}
